package Chapter2;

import java.util.Scanner;

public class Converter {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Fahrenheit: ");

        double fahrenheit = input.nextDouble();
        double celsius = (5.0/9.0) * (fahrenheit - 32);

        System.out.println("Celsius: " + celsius);
    }
}
