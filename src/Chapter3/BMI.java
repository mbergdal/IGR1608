package Chapter3;

import java.util.Scanner;

public class BMI {
    public static void main(String[] args) {
        // Chapter3.BMI = w / h^2
        /*
        Chapter3.BMI < 18.5          Undervektig
        18.5 <= Chapter3.BMI < 25.0  Normal
        25.0 <= Chapter3.BMI < 30.0  Overvektig
        30.0 <= Chapter3.BMI         Fedme
        */

        Scanner input = new Scanner(System.in);

        System.out.println("Vekt: ");
        double weight = 88;//input.nextDouble();

        System.out.println("Høyde: ");
        double height = 1.85; //input.nextDouble();

        double bmi = weight / (height * height);


        weight = 44;

        if (bmi < 18.5){
            System.out.println("undervektig");
        }else if(bmi < 25.0){
            System.out.println("normal");
        }else if (bmi < 31.0){
            System.out.println("overvektig");
        }else{
            System.out.println("fedme");
        }
    }
}
