package Chapter3;

import java.util.Scanner;

public class EvenOrOdd {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter an integer: ");

        int number = input.nextInt();

        // (predikat) ? true : false


        System.out.println((number % 2 == 0) ? "Even" : "Odd");
    }

}
