package Chapter4;

import java.util.Scanner;

public class Strings {
    public static void main(String[] args) {

        String message = "Hello World";

        //String methods
        char c = message.charAt(2);
        System.out.println(c);

        boolean contains = message.contains("K");
        System.out.println(contains);

        //Concatinating strings
        String longString = "Short1" + "Short2";
        longString = "Short1".concat("String2");

        //Convert to lower/upper
        longString = longString.toUpperCase();


        //Read strings from console
        Scanner input = new Scanner(System.in);
        //String line = input.nextLine();

        //Compare strings
        String hello = "Hello";
        String hello1 = "Hello";
        boolean isEqual = hello == hello1;
        isEqual = hello.equals(hello1);

        //Substrings
        String numbers = "0123456789";
        String substring = numbers.substring(2, 3);
        System.out.println(substring);

        //Convert numbers to strings
        String s = "3";
        int i = Integer.parseInt(s);
        String numberString = 3 + "";

        //Formatting console output
        System.out.printf("Hello  %s %s", "World", "again");

        double price = 45.60;
        System.out.printf("The price is %f NOK", price);

    }
}
