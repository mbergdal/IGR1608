package Chapter4;

public class Characters {
    public static void main(String[] args) {
        //What is going on here?
        int i = '2' + '3';
        System.out.println(i);

        //char data type
        char letter = 'A';
        char numChar = '4';

        byte bigE = 69;

        System.out.println(bigE);

        //Encoding: Unicode vs ASCII

        char well = '\u6B22';
        char come = '\u8FCE';
        char question = '\u0191';

        System.out.print(well);
        System.out.println(come);

        //Escape character '\'
        char tab = '\'';
        System.out.println(tab);

        //Casting
        char ch = 65;

        System.out.println(ch);
        //byte b = '\u6B22';
        char a = 0x40;
        System.out.println(a);

        //Numeric operators
        int i1 = '2' + '3';
        System.out.println(i1);

        //Comparing
        boolean bigger = 'a' < 'b';
        System.out.println(bigger);

        //Character methods
        boolean lowerCase = Character.isLowerCase('a');
        System.out.println(lowerCase);
    }
}
