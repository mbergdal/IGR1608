package Chapter9.house;

public class Painter {

    private boolean isFinishedPainting;

    public void paintHouse() {
        isFinishedPainting = true;
    }

    public boolean isFinishedPainting() {
        return isFinishedPainting;
    }
}
