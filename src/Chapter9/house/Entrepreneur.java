package Chapter9.house;

public class Entrepreneur {

    private String name;
    private String orgNumber;

    private Carpenter[] carpenters;
    private Painter[] painters;
    private int progress = 0;

    //Constructor
    public Entrepreneur(String name, String orgNumber){
        this.name = name;
        this.orgNumber = orgNumber;
        createCarpenters();
        createPainters();
    }

    private void createPainters() {
        painters = new Painter[10];
        painters[0] = new Painter();
        painters[1] = new Painter();
        painters[2] = new Painter();
    }

    private void createCarpenters() {
        carpenters = new Carpenter[10];
        carpenters[0] = new Carpenter();
        carpenters[1] = new Carpenter();
    }

    public double getOffer(){
        return 10000.0;
    }

    public void buildHouse() {
        for (Painter p: painters){
            if (p != null)
                p.paintHouse();
        }

        for (Carpenter c: carpenters){
            if (c != null)
                c.build();
        }
    }

    public int getProgress() {
        return progress;
    }

    public void nag() {
        progress++;
    }
}
