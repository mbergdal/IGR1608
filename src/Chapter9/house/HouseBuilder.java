package Chapter9.house;

public class HouseBuilder {
    private static int ACCEPTABLE_PRICE = 10000;

    public static void main(String[] args) {
        //Create an entrepreneur
        Entrepreneur bobTheBuilder = new Entrepreneur("Knut", "934456678");

        //Ask for offer
        double offer = bobTheBuilder.getOffer();

        //Start building
        if (offer <= ACCEPTABLE_PRICE){
            bobTheBuilder.buildHouse();
        }

        //Get progress
        while (bobTheBuilder.getProgress() < 100){
            bobTheBuilder.nag();
        }

        //House is finished!!
        System.out.println("House finished!!");

    }
}
