package Chapter9.house;

public class Carpenter {

    private boolean isFinishedBuilding;

    public Carpenter(){

    }

    public void build() {
        isFinishedBuilding = true;
    }

    public boolean isFinishedBuilding() {
        return isFinishedBuilding;
    }
}
