package Chapter9;

/**
 * Created by mbe on 04/11/2016.
 */
public class CircleTest {
    public static void main(String[] args) {
        Circle c = new Circle(4.0);
        c.setRadius(5.0);

        System.out.println("Radius of Circle is: " + c.getRadius());
        System.out.println("Perimeter of Circle is: " + c.getPerimeter());
    }
}
