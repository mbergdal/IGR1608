package Chapter9.robot;

/**
 * Created by mbe on 03/11/2016.
 */
public class RobotGame {
    public static void main(String[] args) {
        Robot robot = null; // = new Robot("Wall-E", 'N');
        walkCourse(robot);
    }

    private static void walkCourse(Robot robot) {
        robot.walk();

        robot.turnRight();
        robot.walk();
        robot.turnLeft();
        robot.walk();
        robot.walk();
        robot.turnRight();
        robot.walk();
        robot.walk();
        robot.turnRight();
        robot.walk();
        robot.walk();
        robot.walk();
        robot.turnRight();
        robot.walk();
        System.out.println(
                        "I'm " +
                        robot.getName() +
                        ". I am facing " +
                        robot.getCurrentHeading() +
                        ". I have walked " +
                        robot.getDistanceWalked() +
                        " paces.");
    }
}
