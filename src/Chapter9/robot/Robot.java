package Chapter9.robot;

public class Robot {
    private char currentHeading;
    private String name;
    private int distanceWalked;

    public Robot(String name){
        this(name, 'N');
    }

    public Robot(String name, char initialHeading) {
        this.name = name;
        currentHeading = initialHeading;
        distanceWalked = 0;
    }

    public void walk() {
        distanceWalked++;
    }

    public void turnRight() {
        switch (currentHeading){
            case 'N':
                currentHeading = 'E';
                break;
            case 'E':
                currentHeading = 'S';
                break;
            case 'S':
                currentHeading = 'W';
                break;
            case 'W':
                currentHeading = 'N';
                break;
        }

    }

    public void turnLeft() {
        switch (currentHeading){
            case 'N':
                currentHeading = 'W';
                break;
            case 'W':
                currentHeading = 'S';
                break;
            case 'S':
                currentHeading = 'E';
                break;
            case 'E':
                currentHeading = 'N';
                break;
        }
    }

    public String getName() {
        return name;
    }

    public char getCurrentHeading() {
        return currentHeading;
    }


    public int getDistanceWalked() {
        return distanceWalked;
    }
}

    /*private String name;
    private int distanceWalked;
    private char currentHeading;

    public Robot(String name){
        this.name = name;
        this.currentHeading = 'N';
    }

    public Robot(String name, char initialHeading){
        this.name = name;
        this.currentHeading = initialHeading;
    }

    public void walk(){
        distanceWalked++;
    }

    public void turnRight(){
        switch (currentHeading){
            case 'N':
                currentHeading = 'E';
                break;
            case 'E':
                currentHeading = 'S';
                break;
            case 'S':
                currentHeading = 'W';
                break;
            case 'W':
                currentHeading = 'N';
                break;
        }
    }

    public void turnLeft(){
        switch (currentHeading){
            case 'N':
                currentHeading = 'W';
                break;
            case 'W':
                currentHeading = 'S';
                break;
            case 'S':
                currentHeading = 'E';
                break;
            case 'E':
                currentHeading = 'N';
                break;
        }
    }

    public char getCurrentHeading(){
        return currentHeading;
    }

    public int getDistanceWalked(){
        return distanceWalked;
    }

    public String getName(){
        return name;
    }
}*/
