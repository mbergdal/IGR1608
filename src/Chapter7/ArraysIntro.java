package Chapter7;

public class ArraysIntro {

    public static void main(String[] args) {

        declarePrices();

        double[] prices = declarePricesShorthand();
        System.out.println(prices[0]);

        runThroughAnArray();
        //runThroughAnArrayWithForEach();
    }

    private static void declarePrices() {
        int size = 10;
        double[] prices = new double[size]; //NB Fixed Size!
        prices[0] = 100.0;
        prices[3] = 329.0;
        //prices[10] = 330.0;

        //What does the "untouched" places in the array contain?
    }

    private static double[] declarePricesShorthand() {
        double[] prices = {100.0, 329.0}; //NB Fixed Size!
        //prices[2] = 330.0; //ArrayIndexOutOfBoundsException
        return prices;
    }

    private static void runThroughAnArray() {
        char[] letters = {'a', 'b', 'c'};
        for (int i = 0; i < letters.length; i++) {
            System.out.print(letters[i]);
        }
    }

    private static void runThroughAnArrayWithForEach() {
        char[] letters = {'a', 'b', 'c'};
        for(char loo: letters)
            System.out.print(loo);
    }

}
