package Chapter7;

public class CommandLineArguments {
    public static void main(String[] args) {
        if (args.length > 0) {
            System.out.println("You typed: " + args[0]);
        }
    }
}
