package Chapter7;

public class CopyingArrays {

    private static int[] sourceArray = {2, 3, 1, 5, 10};
    private static int[] targetArray = new int[sourceArray.length*2];

    public static void main(String[] args) {
        targetArray = sourceArray;

        copyWithLoop();

        copyWithArraycopy();

        copyWithClone();
    }

    private static void copyWithLoop() {
        for (int i = 0; i < sourceArray.length; i++) {
            targetArray[i] = sourceArray[i];
        }
    }

    private static void copyWithArraycopy() {
        System.arraycopy(sourceArray, 0, targetArray, 0, sourceArray.length);
    }

    private static void copyWithClone() {
        targetArray = sourceArray.clone();
    }
}
