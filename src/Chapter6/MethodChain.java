package Chapter6;

public class MethodChain {
    static int theMagicNumber = 42;

    public static void main(String[] args) {

        int a = 5;
        int b = 6;

        int maxNumber = max(a, b);
        //assignANumber();

        System.out.println("The maximum number squared is: " + square(maxNumber));


    }

    private static String square(int number) {
        double squared = Math.pow(number, 2);
        return squared + "";
    }

    private static int max(int firstNumber, int secondNumber) {
        int result;

        if (firstNumber > secondNumber)
            result = firstNumber;
        else
            result = secondNumber;

        return result;
    }

    private static void assignANumber(){
        theMagicNumber = 77;
    }
}
