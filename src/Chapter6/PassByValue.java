package Chapter6;

public class PassByValue {
    public static void main(String[] args) {
        int a = 42;
        System.out.println("The value of a is: " + a);
        increment(a);
        System.out.println("The value of a is still: " + a);
    }

    private static void increment(int numberToIncrement) {
        numberToIncrement += 1;
        System.out.println("The number is now: " + numberToIncrement);
    }
}
