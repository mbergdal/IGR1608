package Chapter6;

public class Overloading {
    public static void main(String[] args) {
        printMessage("Hello");
        printMessage("Hello", 5);
    }

    private static void printMessage(String message) {

        System.out.println(message);
    }

    //NB names of parameters are important. Shows up in intellisense
    private static void printMessage(String message, int numberOfTimesToPrint) {
        for (int i = 0; i < numberOfTimesToPrint; i++) {
            System.out.println(message);
        }
    }
}
