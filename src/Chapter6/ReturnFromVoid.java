package Chapter6;

public class ReturnFromVoid {
    public static void main(String[] args) {
        doSomething(10);
    }

    private static void doSomething(int theMagicNumber) {
        if (theMagicNumber == 10) {
            System.out.println("Invalid Number");
            return;
        }
        //No need for else-statement... Why?
        System.out.println("The number was: " + theMagicNumber);
    }
}
