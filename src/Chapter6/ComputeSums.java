package Chapter6;

import org.jetbrains.annotations.Contract;

public class ComputeSums {

    public static void main(String[] args) {

        printSum(sum(1, 10));

        System.out.println("Sum from 20 to 37 is: " + sum(20, 37));

        System.out.println("Sum from 35 to 49 is: " + sum(35, 49));

        System.out.println("Sum from 90 to 110 is: " + sum(90, 110));
    }

    private static void printSum(int sumToPrint) {
        System.out.println("The sum is: " + sumToPrint);
    }

    private static int sum(int from, int to){
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += i;
        }

        return sum;
    }
}
